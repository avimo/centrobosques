

<link rel="stylesheet" href="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="/assets/css/pages/portfolio-v2.css">
<link rel="stylesheet" href="/assets/plugins/animate.css">
	<link rel="stylesheet" href="/assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">

	<link rel="stylesheet" href="/assets/plugins/fancybox/source/jquery.fancybox.css">


<div  class="breadcrumbs-v3 wow fadeIn text-center" style="
background-image:    url(http://i.imgur.com/1XfiuOd.jpg);
    background-size:     cover;                     
    background-repeat:   no-repeat;
    background-position: center center;  ">
			<div class="container" id="focus_top">
				<h1 style="text-shadow: 2px 2px #2E2E2E;"><?php echo $title ?></h1>
				<p style="font-size:13px;text-shadow: 2px 2px 4px #000;"><?php echo $intro ?></p>
			</div>
		</div>

<section>
<div class="container content"  >
	<div class="row" >

		<!-- Begin Sidebar Menu -->
		
		<?php foreach ($service->catServiceBanners as $serv): ?>
			<?php foreach ($serv->catServiceContents as $cont): ?>
					<div class="col-md-3 wow fadeInUp">
						<div class="thumbnails thumbnail-style thumbnail-kenburn" data-toggle="modal" data-target="#modal_<?php echo $cont->service_content_id ?>" >
							<div class="caption">
								<h3><a class="hover-effect" href="#"><?php echo $cont->title ?></a></h3>
								<p><?php echo $cont->introduction ?></p>
							</div>
							<div class="thumbnail-img" >
								<div class="overflow-hidden">
									<img class="img-responsive" src="<?php echo $cont->img_intro ?>" alt="" style="width:100%"/>
								</div>
									<a class="btn-more hover-effect"  href="#">Ver más <i class="fa fa-plus-square"></i></a>
							</div>
								
						</div>
					</div>
								
					<div class="modal fade" id="modal_<?php echo $cont->service_content_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 id="myModalLabel1" class="modal-title"><?php echo $cont->title ?></h4>
								</div>
								<div class="modal-body" style="font-size:16px;">
								<p style="font-size:28px;">
										<?php 
											$content_str=$cont->service_content;
											for ($i=0; $i < 100; $i++) { 
												$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
											}
										 ?>
										<?php echo $content_str; ?>
										<?php if ($cont->img_description): ?>
												<img src="<?php echo $cont->img_description ?> " class="img-responsive" style="width:80%;margin:0 auto">
										<?php endif ?>
										<div class="shadow-wrapper " style="width:80%;margin:0 auto">
											<div class="box-shadow shadow-effect-2">
												<?php if ($cont->img_paragraph): ?>
													<img src="<?php echo $cont->img_paragraph ?> " class="img-responsive img-bordered" style="width:100%">
												<?php endif ?>
												
											</div>
										</div>


									
								</p>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn-u btn-u-default" type="button">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
				
			<?php endforeach ?>
		
		<?php endforeach ?>
			

		
		<!-- End Content -->
	</div>
</div>

</section>
<script type="text/javascript" src="/assets/js/app.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/style-switcher.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/fancy-box.js"></script>
<script type="text/javascript">

jQuery(document).ready(function() {
FancyBox.initFancybox();

});
</script>

