<?php 
//session_start();
$slider=CatSlider::model()->find(array('condition'=>"tipo_id=1 and language_id=".$_SESSION['lang_selected']));

//echo "<script>console.log('".print_r($slider)."')</script>";
//return 0;
 ?>
<?php if ($slider): ?>

		
			<!-- masterslider -->
			<div class="master-slider ms-skin-black-2 round-skin" id="masterslider" 
			>
			<?php foreach ($slider->catImgs as $img ): ?>
				<div class="ms-slide" style="z-index: 10" data-delay="6">
						<img 	src="/assets/plugins/master-slider/masterslider/style/blank.gif" 
								data-src="<?php echo $img->img_link ?>" alt="">
						

						<div 	class="ms-layer ms-promo-info ms-promo-info-in color-light"

								data-effect="bottom(40)"
								data-duration="2000"
								data-delay="100"
								data-ease="easeOutExpo"
								style="top:100px;text-shadow: 1px 1px 9px rgba(0, 0, 0, 1);"
						>
							<?php echo $img->title ?> 
						</div>

						<div class="ms-layer ms-promo-sub color-light" style="left:15px; 
						top:200px;text-shadow: 1px 1px 9px rgba(0, 0, 0, 1);font-size:25px"
						data-effect="bottom(40)"
						data-duration="2000"
						data-delay="100"
						data-ease="easeOutExpo"
						>

							<?php echo $img->subtitle ?>
						</div>

						<?php if ($_SESSION['lang_selected']==1): ?>
							<a class="ms-layer btn-u" style="left:15px; top:280px" href="/site/page/site_offices"
							data-effect="bottom(40)"
							data-duration="2000"
							data-delay="1300"
							data-ease="easeOutExpo"
							>OFICINAS</a>

							<a class="ms-layer btn-u btn-u-dark" style="left:150px; top:280px" href="/site/page/site_events"
							data-effect="bottom(40)"
							data-duration="2000"
							data-delay="1300"
							data-ease="easeOutExpo"
							>EVENTOS</a>
						<?php endif ?>
						<?php if ($_SESSION['lang_selected']==2): ?>
							<a class="ms-layer btn-u" style="left:15px; top:280px" href="/site/page/site_offices"
							data-effect="bottom(40)"
							data-duration="2000"
							data-delay="1300"
							data-ease="easeOutExpo"
							>OFFICES</a>

							<a class="ms-layer btn-u btn-u-dark" style="left:150px; top:280px" href="/site/page/site_events"
							data-effect="bottom(40)"
							data-duration="2000"
							data-delay="1300"
							data-ease="easeOutExpo"
							>EVENTS</a>
						<?php endif ?>
						
					</div>
				
			<?php endforeach ?>
			</div>
			<!-- end of masterslider -->
		</div>




<?php endif ?>
<script type="text/javascript">

		jQuery(document).ready(function() {
			$('#inicio').addClass("active")
		});

</script>
<div class="call-action-v1 bg-color-light">
	<div class="container">
		<div class="call-action-v1-box">
			<div class="call-action-v1-in">
				<?php if ($_SESSION['lang_selected']==1): ?>
					<h1>Centro Bosques | <small>Centro de Negocios</small></h1>
				<?php endif ?>
				
				<?php if ($_SESSION['lang_selected']==2): ?>
					<h1>Centro Bosques | <small>Business center</small></h1>
				<?php endif ?>
			</div>
			<div class="call-action-v1-in inner-btn page-scroll">
				<a href="/site/page/site_contact" class="btn-u btn-u">Contacto</a>
			</div>
		</div>
	</div>
</div>
<!--=== Slider ===-->
		
		<!--=== End Slider ===-->