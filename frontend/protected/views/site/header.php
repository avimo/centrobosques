<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->


<head>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>
	<link rel="stylesheet" href="/assets/plugins/hover-effects/css/custom-hover-effects.css">
	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="/assets/css/headers/header-v6.css">
	<link rel="stylesheet" href="/assets/css/footers/footer-v6.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="/assets/plugins/animate.css">
	<link rel="stylesheet" href="/assets/plugins/line-icons/line-icons.css">
	<link href="/assets/awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="/assets/plugins/fancybox/source/jquery.fancybox.css">
	<link rel="stylesheet" href="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="/assets/plugins/master-slider/masterslider/style/masterslider.css">
	<link rel='stylesheet' href="/assets/plugins/master-slider/masterslider/skins/black-2/style.css">

	<!-- CSS Pages Style -->
	<link rel="stylesheet" href="/assets/css/pages/page_one.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="/assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="/assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="/assets/css/custom.css">
</head>



<!-- JS Global Compulsory -->
	<script src="/assets/plugins/jquery/jquery.min.js"></script>
	<script src="/assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script src="/assets/plugins/smoothScroll.js"></script>
	<script src="/assets/plugins/jquery.easing.min.js"></script>
	<script src="/assets/plugins/pace/pace.min.js"></script>
	<script src="/assets/plugins/jquery.parallax.js"></script>
	<script src="/assets/plugins/counter/waypoints.min.js"></script>
	<script src="/assets/plugins/counter/jquery.counterup.min.js"></script>
	<script src="/assets/plugins/owl-carousel/owl.carousel.js"></script>
	<script src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="/assets/plugins/revolution-slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script src="/assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>

	<script src="/assets/plugins/modernizr.js"></script>
	<script src="/assets/plugins/login-signup-modal-window/js/main.js"></script> <!-- Gem jQuery -->

	<!-- JS Page Level-->
	<script src="/assets/js/one.app.js"></script>
	<script src="/assets/js/forms/login.js"></script>
	
	<script src="/assets/js/plugins/pace-loader.js"></script>
	<script src="/assets/js/plugins/owl-carousel.js"></script>
	<script src="/assets/js/plugins/style-switcher.js"></script>
	<script src="/assets/js/plugins/revolution-slider.js"></script>
	<script src="/assets/js/plugins/cube-portfolio/cube-portfolio-lightbox.js"></script>

	<script type="text/javascript" src="/assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="/assets/plugins/smoothScroll.js"></script>
	<script src="/assets/plugins/ladda-buttons/js/spin.min.js"></script>
	<script src="/assets/plugins/ladda-buttons/js/ladda.min.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="/assets/js/plugins/ladda-buttons.js"></script>
	
	<link rel="stylesheet" href="/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
	<link rel="stylesheet" href="/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
	<!--[if lt IE 9]><link rel="stylesheet" href="/assets/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css"><![endif]-->

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="/assets/css/pages/page_contact.css">
	<!-- CSS Customization -->

	<!-- JS Implementing Plugins -->
	<script src="/assets/plugins/back-to-top.js"></script>
	<script src="/assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
	<script src="/assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
	
	<!-- JS Customization -->
	<script src="/assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script src="/assets/js/plugins/google-map.js"></script>
	<script src="/assets/js/pages/page_contact_advanced.js"></script>

	