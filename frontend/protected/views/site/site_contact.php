	
<?php $contact=CatContact::model()->find("language_id=".$_SESSION['lang_selected']." and status=TRUE") ?>

<?php if ($contact): ?>
<section id="contacto" class="">
<div  class="breadcrumbs-v3  text-center" style="
background-image:    url(http://i.imgur.com/qMcengf.jpg);
background-size:     cover;                     
background-repeat:   no-repeat;
background-position: center center;  ">
	<div class="container" id="focus_top">
		<h1><?php echo $contact->title ?></h1>
		

	</div>

</div>
<div class="container content">
	<div class="row margin-bottom-30">
		<div class="col-md-9 mb-margin-bottom-30">
			
			<form action="/site/contact" method="post" id="sky-form3" class="sky-form sky-changes-3">
				<fieldset>
					<div class="row">
						<section class="col col-6">
							<label class="label"><?php echo $contact->customer_name ?></label>
							<label class="input">
								<i class="icon-append fa fa-user"></i>
								<input type="text" name="name" id="name"
								
								>

							</label>
						</section>
						<section class="col col-6">
							<label class="label"><?php echo $contact->customer_email ?></label>
							<label class="input">
								<i class="icon-append fa fa-envelope-o"></i>
								<input type="email" name="email" id="email"
								>
							</label>
						</section>
					</div>

					<section>
						<label class="label"><?php echo $contact->customer_subject ?></label>
						<label class="input">
							<i class="icon-append fa fa-tag"></i>
							<input type="text" name="subject" id="subject"
							>
						</label>
					</section>

					<section>
						<label class="label"><?php echo $contact->customer_message ?></label>
						<label class="textarea">
							<i class="icon-append fa fa-comment"></i>
							<textarea rows="4" name="message" id="message"></textarea>
						</label>
					</section>

				</fieldset>

				<footer>
					<button type="submit" class="btn-u"><?php echo $contact->title ?></button>
				</footer>

				<div class="message">
					<i class="rounded-x fa fa-check"></i>
					<p>Your message was successfully sent!</p>
				</div>
			</form>
		</div><!--/col-md-9-->

		<div class="col-md-3">
			<!-- Contacts -->
			<div class="headline"><h2><?php echo $contact->title ?></h2></div>
			<ul class="list-unstyled who margin-bottom-30">
				<li><a href="#"><i class="fa fa-home"></i><?php echo $contact->company_address ?></a></li>
				<li>
					<a href="#">
						<i class="fa fa-envelope"></i> ventas@centrobosques.com 
						<br>
						<i class="fa fa-envelope"></i> victor_deleon@centrobosques.com
					</a>
				
				</li>
				<li><a href="#"><i class="fa fa-phone"></i><?php echo $contact->company_phone ?></a></li>
			</ul>

			
		</div><!--/col-md-3-->
	</div><!--/row-->
</div><!--/container-->
		<!--=== End Content Part ===-->

		<!-- Google Map -->
		<div id="map" class="map">
		</div><!---/map-->
		<!-- End Google Map -->

</section>


<script>
		jQuery(document).ready(function() {
			PageContactForm.initPageContactForm();
		});

		// Google Map
		function initMap() {
			GoogleMap.initGoogleMap(19.4237628,-99.2071425);
		}
	</script>

	<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCDSb3wORiw36c9kGhpSVqjkTYtJpVp4l4&callback=initMap" async defer></script>	
<?php endif ?>
