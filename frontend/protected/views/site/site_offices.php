<?php $section=CatSection::model()->find("description='offices' and language_id=".$_SESSION['lang_selected']." and status=TRUE") ?>
<?php if ($section): ?>
<?php $content=$section->catContents; ?>
<section id="oficinas">
	<div class="row">
<?php foreach ($content as $c ): ?>
				<div  class="breadcrumbs-v3  text-center" style="
background-image:    url(http://i.imgur.com/M9YuF1b.jpg);
    background-size:     cover;                     
    background-repeat:   no-repeat;
    background-position: center center;  ">
			<div class="container" id="focus_top">
				<h1><?php echo $c->title ?></h1>
				

			</div>
			</div>
	<div class="container content-sm">
		


		<?php endforeach ?>
		
		
			<?php $content=CatContent::model()->findAll(
				array("condition"=>" section_id in (5,12,11) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(5,6,7,8,26,27,28)",
					"order"=>"sorting")

			); ?>
			<div class="row">
				<?php foreach ($content as $of ): ?>
					<div class="col-sm-4 md-margin-bottom-40" >
						<div class="tag-box tag-box-v2 box-shadow shadow-effect-1" style="height:600px" >
							<div class="thumbnail-img">
								<img class="img-responsive" src="<?php echo $of->introduction ?>" alt="">
							</div>
							<div class="caption" >
								<h3><a ><?php echo $of->title ?></a></h3>
								<?php 
									$content_str=$of->content;
									for ($i=0; $i < 100; $i++) { 
										$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
									}
								 ?>
								
								<p><?php echo $content_str; ?></p>
								<br>
								<a class="btn-u btn-u-dark" onclick= "location.href='/site/page/site_contact'" style="cursor:pointer;margin-top:10px	"

							
							>CONTACTO</a>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
			<div class="row">
				<?php if ($_SESSION['lang_selected']==1): ?>
				<div class="col-lg-12">
					<center><h1>Oficinas Virtuales</h1></center>
					<hr>
				</div>
				<?php endif ?>
			
				<?php if ($_SESSION['lang_selected']==2): ?>
				<div class="col-lg-12">
					<center><h1>Virtual Offices</h1></center>
					<hr>
				</div>
				<?php endif ?>	
				<?php $content=CatContent::model()->findAll(
					array("condition"=>" section_id in (5,12,11) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(53,54,55,56,57,58)",
						"order"=>"sorting")

				); ?>
				<?php foreach ($content as $of ): ?>
					<div class="col-sm-4 md-margin-bottom-40" >
						<div class="tag-box tag-box-v2 box-shadow shadow-effect-1" style="" >
							<div class="thumbnail-img">
								<img class="img-responsive" src="<?php echo $of->introduction ?>" alt="">
							</div>
							<div class="caption" >
								<h3><a ><?php echo $of->title ?></a></h3>
								<?php 
									$content_str=$of->content;
									for ($i=0; $i < 100; $i++) { 
										$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
									}
								 ?>
								
								<p><?php echo $content_str; ?></p>
								<a class="btn-u btn-u-dark" href="/site/page/site_contact" onclick= "location.href='/site/page/site_contact'" style="cursor:pointer"
							
							>CONTACTO</a>
							</div>
							
						</div>
					</div>
				<?php endforeach ?>
			</div>


			<div class="row">
				<?php if ($_SESSION['lang_selected']==1): ?>
				<div class="col-lg-12">
					<center><h1>Sala de Juntas</h1></center>
					<hr>
				</div>
				<?php endif ?>
			
				<?php if ($_SESSION['lang_selected']==2): ?>
				<div class="col-lg-12">
					<center><h1>Meeting Room</h1></center>
					<hr>
				</div>
				<?php endif ?>	
				<?php $content=CatContent::model()->findAll(
					array("condition"=>" section_id in (16,17) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(59,60)",
						"order"=>"sorting")

				); ?>
				<?php foreach ($content as $of ): ?>
					<div class="col-sm-4 md-margin-bottom-40" >
						<div class="tag-box tag-box-v2 box-shadow shadow-effect-1" style="" >
							<div class="thumbnail-img">
								<img class="img-responsive" src="<?php echo $of->introduction ?>" alt="">
							</div>
							<div class="caption" >
								<h3><a ><?php echo $of->title ?></a></h3>
								<?php 
									$content_str=$of->content;
									for ($i=0; $i < 100; $i++) { 
										$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
									}
								 ?>
								
								<p><?php echo $content_str; ?></p>
								<a class="btn-u btn-u-dark" href="/site/page/site_contact" onclick= "location.href='/site/page/site_contact'" style="cursor:pointer"
							
							>CONTACTO</a>
							</div>
							
						</div>
					</div>
				<?php endforeach ?>
			</div>



			
			
			
		</div>
	</div>

</section>
<?php endif ?>