<?php $content=CatContent::model()->find(
	array("condition"=>"title='Catering' and language_id=".$_SESSION['lang_selected']." ",
		"order"=>"sorting asc"
		)
) ?>
<?php 
//var_dump((json_decode($content->content)));
$json = $content->content;

$menus=json_decode($json, true);
$keys=array_keys($menus);
?>

<!-- Begin Sidebar Menu -->
				<div class="col-md-3">
					<ul class="list-group sidebar-nav-v1" id="sidebar-nav">
						<!-- Typography -->
						<?php foreach ($keys as $k ): ?>
							<li class="list-group-item list-toggle">
								<a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-<?php echo $k ?>"><?php echo $k ?></a>
								<ul id="collapse-<?php echo $k ?>" class="collapse">
									<?php foreach ($menus[$k] as $m): ?>
										<li>
											<a class="collapse-link" show-element="<?php echo $m ?>">
												<?php echo $m ?>
											</a>
										</li>
									<?php endforeach ?>
									
								</ul>
							</li>
						<?php endforeach ?>
						
						<!-- End Charts -->
					</ul>
				</div>
				<?php foreach ($keys as $k ): ?>
					<?php foreach ($menus[$k] as $m): ?>
						<?php $div=CatContent::model()->find("title='".$m."' and language_id=".$_SESSION['lang_selected']." ") ?>
						<?php if ($div): ?>
							<div class="col-md-4 desplegable" div-element="<?php echo $m ?>" style="display:none">
								<?php echo  $div->content?>
									
							</div>
						<?php endif ?>
					<?php endforeach ?>
					
				<?php endforeach ?>
				<div class="col-md-4">
					<img src="http://i.imgur.com/IeSXH8R.png" class="pull-left lft-img-margin img-width-200">
				</div>
				<!-- End Sidebar Menu -->


<script type="text/javascript">

jQuery(document).ready(function() {
	$(".desplegable").first().fadeIn()
	$(".collapse-link").on('click',function(){
		$(".desplegable").css("display","none");
		$("div[div-element='"+$(this).attr("show-element")+"']").fadeIn()
	})
});
</script>