


<?php 
 

?>
<?php if (isset($_SESSION['lang_selected']) && $_SESSION['lang_selected']==1){ ?>
	<!-- <div class="lang-block">
		<a href="#" class="mega-hover__current" aria-haspopup="true">Es
			<img src="/assets/flags/mex.png" style="height:15px">
		</a>
		<ul class="mega-hover-list">
			<li>
				<a href="#">En
					<img src="/assets/flags/eng.png" style="height:15px">
				</a>
			</li>
		</ul>
	</div> -->


<!--=== Style Switcher ===-->
	<i class="style-switcher-btn ">
		<img src="/assets/flags/mex.png" style="height:20px">
		ES
	</i>
	<div class="style-switcher animated fadeInRight" style="width:150px">
		<div class="style-swticher-header">
			<div class="style-switcher-heading">Language</div>
			<div class="theme-close"><i class="icon-close"></i></div>
		</div>

		<div class="style-swticher-body">
			<!-- Theme Colors -->
			<ul class="list-unstyled">
				<li style="background:white;padding:3px">
					<a href="/site/setLang/1">
						<img src="/assets/flags/mex.png" style="height:20px">
					</a>
				</li>
				<li style="background:white;padding:3px">
					<a href="/site/setLang/2">
						<img src="/assets/flags/eng.png" style="height:20px">
					</a>
				</li>
			</ul>

			
		</div>
	</div><!--/style-switcher-->
	<!--=== End Style Switcher ===-->

<?php } else if (isset($_SESSION['lang_selected']) && $_SESSION['lang_selected']==2){ ?>
	<!-- <div class="lang-block">
		<a href="#" class="mega-hover__current" aria-haspopup="true">En
			<img src="/assets/flags/eng.png" style="height:15px">
		</a>
		<ul class="mega-hover-list">
			<li>
				<a href="/site/setLang/1">Es
					<img src="/assets/flags/mex.png" style="height:15px">
				</a>
			</li>
		</ul>
	</div> -->
	<i class="style-switcher-btn ">
		<img src="/assets/flags/eng.png" style="height:20px">
		EN
	</i>
	<div class="style-switcher animated fadeInRight" style="width:150px">
		<div class="style-swticher-header">
			<div class="style-switcher-heading">Language</div>
			<div class="theme-close"><i class="icon-close"></i></div>
		</div>

		<div class="style-swticher-body">
			<!-- Theme Colors -->
			<ul class="list-unstyled">
				<li style="background:white;padding:3px">
					<a href="/site/setLang/1">
						<img src="/assets/flags/mex.png" style="height:20px">
					</a>
				</li>
				<li style="background:white;padding:3px">
					<a href="#">
						<img src="/assets/flags/eng.png" style="height:20px">
					</a>
				</li>
			</ul>

			
		</div>
	</div><!--/style-switcher-->
<?php }else{ 
	$_SESSION['lang_selected']=1;
		?>
	<i class="style-switcher-btn ">
		<img src="/assets/flags/mex.png" style="height:20px">
		ES
	</i>
	<div class="style-switcher animated fadeInRight" style="width:150px">
		<div class="style-swticher-header">
			<div class="style-switcher-heading">Language</div>
			<div class="theme-close"><i class="icon-close"></i></div>
		</div>

		<div class="style-swticher-body">
			<!-- Theme Colors -->
			<ul class="list-unstyled">
				<li style="background:white;padding:3px">
					<a href="/site/setLang/1">
						<img src="/assets/flags/mex.png" style="height:20px">
					</a>
				</li>
				<li style="background:white;padding:3px">
					<a href="#">
						<img src="/assets/flags/eng.png" style="height:20px">
					</a>
				</li>
			</ul>

			
		</div>
	</div><!--/style-switcher-->
<?php } ?>