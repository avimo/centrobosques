<?php $section=CatSection::model()->find("description='services' and language_id=".$_SESSION['lang_selected']." and status=TRUE") ?>
<?php if ($section): ?>
<?php $content=$section->catContents; ?>
<section id="services" class="md-margin-bottom-40">
	<div  class="breadcrumbs-v3  text-center" style="
background-image:    url(http://i.imgur.com/748cHgF.jpg);
    background-size:     cover;                     
    background-repeat:   no-repeat;
    background-position: center center;  margin-bottom:20px">
                        <div class="container" id="focus_top">
                                <h1 style="text-shadow: 2px 2px #2E2E2E;margin-bottom:20px;"><?php echo $content[0]->title ?></h1>

                        </div>

        </div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 wow fadeInUp">
				<div class="thumbnail box-shadow-outset">
					<a class="fancybox-button zoomer" data-rel="fancybox-button" title="Generales" href="/site/Generales">
						<span class="overlay-zoom">
							<img class="img-responsive" src="http://i.imgur.com/ZnXLOAM.png" alt="" style="width:100%">
							<span class="zoom-icon"></span>
						</span>
					</a>
					<div class="caption">
						<h3><a class="hover-effect" href="#"><?php echo $content[1]->title ?></a></h3>
						<p><a href="/site/Generales" class="btn-u btn-u-small">Ver servicios <i class="fa fa-external-link-square" aria-hidden="true"></i></a></p>
					</div>
				</div>
			</div>
			<div class="col-md-6 wow fadeInUp">
				<div class="thumbnail box-shadow-outset">
					<a class="fancybox-button zoomer" data-rel="fancybox-button" title="Especiales" href="/site/Especiales">
						<span class="overlay-zoom">
							<img class="img-responsive" src="http://i.imgur.com/MZF9lTT.png" alt="" style="width:100%">
							<span class="zoom-icon"></span>
						</span>
					</a>
					<div class="caption">
						<h3><a class="hover-effect" href="#"><?php echo $content[2]->title ?></a></h3>
						<p><a href="/site/Especiales" class="btn-u btn-u-small">Ver servicios <i class="fa fa-external-link-square" aria-hidden="true"></i></a></p>
					</div>
				</div>
			</div>

			
		</div>
	</div>	
		



	<div class="title-v1">
			
	</div>
</section>
<?php endif ?>
