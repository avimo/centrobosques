<?php $benefit=CatBenefitContent::model()->findAll("language_id=".$_SESSION['lang_selected']." and status=TRUE") ?>
<?php if ($benefit): ?>
	<section id="beneficios">
	<div  class="breadcrumbs-v3  text-center" style="
background-image:    url(http://i.imgur.com/cxcMrtZ.jpg);
    background-size:     cover;                     
    background-repeat:   no-repeat;
    background-position: center center;  ">
			<div class="container" id="focus_top">
				<h1 style="text-shadow: 2px 2px #2E2E2E;">Beneficios</h1>
				<p style="font-size:13px;font-size:13px;text-shadow: 2px 2px 4px #000;">Dedique su tiempo a hacer crecer su empresa y dejenos el resto a nosotros. 
a nosotros.</p>

			</div>

	</div>
	<div class="row" style="margin-top:30px">
		<div class="container">
			<?php foreach ($benefit as $ben): ?>
			
				<div class="col-md-6 wow fadeInRight"  >
					<div class="tag-box tag-box-v2 box-shadow shadow-effect-1" style="height:150px">
						<h2><i class="fa fa-check-circle" aria-hidden="true"></i> <?php echo $ben->title ?></h2>
						<p><?php echo $ben->benefit_content ?></p>
					</div>
				</div>
				
			<?php endforeach ?>
		</div>
		
	</div>

</section>
<?php endif ?>

