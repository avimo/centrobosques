<?php 
	
	
	$options=CatTopBanner::model()->findAll(array('order'=>'sorting',
		'condition'=>'language_id='.$_SESSION['lang_selected']." and top_banner_parent_id is null and status=TRUE")); 
		
?>

<ul class="nav navbar-nav">
	<?php foreach ($options as $opt): ?>

			
			<?php 
			$location=$opt->link;
			if ($opt->land && $opt->land->page_ref!='index') {
				$location='/site/landing/'.$opt->land->page_ref;
			} ?>
			<script type="text/javascript">
				console.log("<?php echo $location ?>")
			</script>
			<li id="<?php echo $opt->description ?>" class="page-scroll home <?php echo $opt->css ?>" onclick="location.href = ' <?php echo $location ?>';">
				<a href="<?php echo $location ?>" <?php if ($opt->css) {?> class="dropdown-toggle" data-toggle="dropdown"<?php } ?> >
					<?php echo $opt->name ?></a>
				<?php if ($opt->css): ?>
					<ul class="dropdown-menu">
						<?php foreach ($opt->catTopBanners as $ban): ?>
							<li><a href="/site/page/<?php echo $ban->land->page_ref ?>"><?php echo $ban->name ?></a></li>
						<?php endforeach ?>
						
					</ul>
				<?php endif ?>
			</li>

		
		
	<?php endforeach ?>
</ul>