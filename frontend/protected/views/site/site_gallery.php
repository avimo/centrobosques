
<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="/assets/plugins/animate.css">
	<link rel="stylesheet" href="/assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="/assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/plugins/fancybox/source/jquery.fancybox.css">

<?php 


$images=[
"http://i.imgur.com/bGPShSJ.jpg",
"http://i.imgur.com/XvWvaAd.jpg",
"http://i.imgur.com/67dQFJF.jpg",
"http://i.imgur.com/bKL7RaX.jpg",
"http://i.imgur.com/wwDyqwI.jpg",
"http://i.imgur.com/y7dkhNz.jpg",
"http://i.imgur.com/Vgsn5eh.jpg",
"http://i.imgur.com/fSXflfP.jpg",
"http://i.imgur.com/XofJWJh.jpg",
"http://i.imgur.com/Uub2LqJ.jpg",
"http://i.imgur.com/vdLCjgP.jpg",
"http://i.imgur.com/EcHW3mY.jpg"
]
 ?>


<div class="container content">
	<?php if ($_SESSION['lang_selected']==1): ?>
		<div class="text-center margin-bottom-50">
			<h2 class="title-v2 title-center">Galería</h2>
		</div>
	<?php endif ?>
	
	<?php if ($_SESSION['lang_selected']==2): ?>
		<div class="text-center margin-bottom-50">
			<h2 class="title-v2 title-center">Gallery</h2>
		</div>
	<?php endif ?>	
			

			<div class="row  margin-bottom-30">
				<?php for ($i=0; $i < 12; $i++) { 
					$num=rand(0,11);
				?> 
					

					<div class="col-sm-3 sm-margin-bottom-30" style="margin-bottom:30px">
						<a href="<?php echo $images[$i] ?>" rel="gallery2" class="fancybox img-hover-v1" >
							<span><img class="img-responsive" src="<?php echo $images[$i] ?>" alt=""></span>
						</a>
					</div>
				<?php
				} ?>

				
			</div>

			

	<script type="text/javascript" src="/assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="/assets/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

	<script type="text/javascript" src="/assets/js/app.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/fancy-box.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/style-switcher.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			FancyBox.initFancybox();
			StyleSwitcher.initStyleSwitcher();
		});
	</script>