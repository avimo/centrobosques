<style type="text/css"></style>
<?php $section=CatSection::model()->find("description in ('Eventos') and language_id=".$_SESSION['lang_selected']." and status=TRUE") ?>
<?php if ($section): ?>

	<div  class="breadcrumbs-v3 wow fadeIn text-center" style="
background-image:    url(http://i.imgur.com/cxcMrtZ.jpg);
    background-size:     cover;                     
    background-repeat:   no-repeat;
    background-position: center center;  ">
			<div class="container" id="focus_top">
				<h1><?php echo $section->title ?></h1>
				
			</div>
		</div>
	
<section id="eventos">

<div class="container content">




	
		<div class="row">
			<?php $content=CatContent::model()->findAll(
				array("condition"=>" section_id in (6,7,13,14) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(9,10,11,12,29,30,32,31)",
					"order"=>"sorting")

			); ?>
			<?php foreach ($content as $cont): ?>
				<div class="col-md-3 wow fadeInUp" data-toggle="modal" data-target="#modal_<?php echo $cont->content_id ?>">
						<div class="caption" style="height:120px">
							<h3><?php echo $cont->title ?></h3>
							<p><?php echo $cont->introduction ?></p>
						</div>
						<div class="thumbnails thumbnail-style thumbnail-kenburn">
							<div class="thumbnail-img">
								<div class="overflow-hidden">
									<img class="img-responsive" src="<?php echo $cont->img_intro ?>" alt="" style="width:100%"/>
								</div>
									<a class="btn-more hover-effect" >Ver más <i class="fa fa-plus-square"></i></a>
						</div>
								
						</div>
				</div>
								
					<div class="modal fade" id="modal_<?php echo $cont->content_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 id="myModalLabel1" class="modal-title"><?php echo $cont->title ?></h4>
								</div>
								<div class="modal-body" style="font-size:16px">
								<p style="font-size:24px;">
										<?php 
											$content_str=$cont->content;
											for ($i=0; $i < 100; $i++) { 
												$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
											}
										 ?>
										<?php echo $content_str; ?>
										<?php if ($cont->img_description): ?>
												<img src="<?php echo $cont->img_description ?> " class="img-responsive" style="width:80%;margin:0 auto;max-height:200px">
										<?php endif ?>
										<div class="shadow-wrapper " style="width:80%;margin:0 auto">
											<div class="box-shadow shadow-effect-2">
												<?php if ($cont->img_paragraph): ?>
													<br><img src="<?php echo $cont->img_paragraph ?> " class="img-responsive img-bordered" style="width:100%"<>
												<?php endif ?>
												
											</div>
										</div>


									
								</p>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn-u btn-u-default" type="button">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
			<?php endforeach ?>
		</div>
		<div class="row">
			<?php $content=CatContent::model()->findAll(
				array("condition"=>" section_id in (6,7,13,14) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(14,41,34,47)",
					"order"=>"sorting")

			); ?>
			

			<?php if ($_SESSION['lang_selected']==1): ?>
				<div class="col-lg-12">
					<hr>
					<center><h1>Catering | Banquetes </h1><br>
						<p style="font-size:18px">Nuestro servicio de catering, le provee una experiencia culinaria unica, gracias a nuestra amplia gama de platillos y bebidas, pensados para dar un toque especial a su evento. Las opciones que ofrecemos son las siguientes:</p>
					<hr>
					<h3>Canapés básicos</h3>
						<p style="font-size:18px">10 Canapes a elegir entre dulces y Salados</p>
					</center>
				</div>
			<?php endif ?>
			
			<?php if ($_SESSION['lang_selected']==2): ?>
				<div class="col-lg-12">
					<hr>
					<center><h1>Catering | Banquet </h1><br>
						<p style="font-size:18px">Our catering service provides a unique culinary experience, thanks to great options of dishes and drinks, planed to give that especial touch to your event.</p>
					<hr>
					<h3>Basic Canapes</h3>
						<p style="font-size:18px">Choose 10 canapes between savory and sweet.</p>
				</center>
				</div>
			<?php endif ?>
			<?php foreach ($content as $cont): ?>
				<div class="col-md-3 wow fadeInUp" data-toggle="modal" data-target="#modal_<?php echo $cont->content_id ?>">
						<div class="caption" style="height:120px">
							<h3><?php echo $cont->title ?></h3>
							<p><?php echo $cont->introduction ?></p>
						</div>
						<div class="thumbnails thumbnail-style thumbnail-kenburn">
							<div class="thumbnail-img">
								<div class="overflow-hidden">
									<img class="img-responsive" src="<?php echo $cont->img_intro ?>" alt="" style="width:100%"/>
								</div>
									<a class="btn-more hover-effect">Ver más <i class="fa fa-plus-square"></i></a>
						</div>
								
						</div>
				</div>
								
					<div class="modal fade" id="modal_<?php echo $cont->content_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 id="myModalLabel1" class="modal-title"><?php echo $cont->title ?></h4>
								</div>
								<div class="modal-body" style="font-size:16px">
								<p style="font-size:24px;height:">
										<?php 
											$content_str=$cont->content;
											for ($i=0; $i < 100; $i++) { 
												$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
											}
										 ?>
										<?php echo $content_str; ?>
										<?php if ($cont->img_description): ?>
												<img src="<?php echo $cont->img_description ?> " class="img-responsive" style="width:80%;margin:0 auto;max-height:200px">
										<?php endif ?>
										<div class="shadow-wrapper " style="width:80%;margin:0 auto">
											<div class="box-shadow shadow-effect-2">
												<?php if ($cont->img_paragraph): ?>
													<br><img src="<?php echo $cont->img_paragraph ?> " class="img-responsive img-bordered" style="width:100%"<>
												<?php endif ?>
												
											</div>
										</div>


									
								</p>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn-u btn-u-default" type="button">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
			<?php endforeach ?>
		</div>
		<div class="row">
			<?php $content=CatContent::model()->findAll(
				array("condition"=>" section_id in (6,7,13,14) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(15,42,35,48)",
					"order"=>"sorting")

			); ?>

			<?php if ($_SESSION['lang_selected']==1): ?>
				<div class="col-lg-12">
					<hr>
					<center><h3>Canapés Medios</h3>
						<p style="font-size:18px">10 Canapes a elegir entre dulces y Salados</p>
					</center>
				</div>
			<?php endif ?>
			
			<?php if ($_SESSION['lang_selected']==2): ?>
				<div class="col-lg-12">
					<hr>
					<center><h3>Medium Canapes</h3>
						<p style="font-size:18px">Choose 10 canapes between savory and sweet.</p>
					</center>
					
				</div>
			<?php endif ?>
			
			<?php foreach ($content as $cont): ?>
				<div class="col-md-3 wow fadeInUp" data-toggle="modal" data-target="#modal_<?php echo $cont->content_id ?>">
						<div class="caption" style="height:120px">
							<h3><?php echo $cont->title ?></h3>
							<p><?php echo $cont->introduction ?></p>
						</div>
						<div class="thumbnails thumbnail-style thumbnail-kenburn">
							<div class="thumbnail-img">
								<div class="overflow-hidden">
									<img class="img-responsive" src="<?php echo $cont->img_intro ?>" alt="" style="width:100%"/>
								</div>
									<a class="btn-more hover-effect">Ver más <i class="fa fa-plus-square"></i></a>
						</div>
								
						</div>
				</div>
								
					<div class="modal fade" id="modal_<?php echo $cont->content_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 id="myModalLabel1" class="modal-title"><?php echo $cont->title ?></h4>
								</div>
								<div class="modal-body" style="font-size:16px">
								<p style="font-size:24px;">
										<?php 
											$content_str=$cont->content;
											for ($i=0; $i < 100; $i++) { 
												$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
											}
										 ?>
										<?php echo $content_str; ?>
										<?php if ($cont->img_description): ?>
												<img src="<?php echo $cont->img_description ?> " class="img-responsive" style="width:80%;margin:0 auto;max-height:200px">
										<?php endif ?>
										<div class="shadow-wrapper " style="width:80%;margin:0 auto">
											<div class="box-shadow shadow-effect-2">
												<?php if ($cont->img_paragraph): ?>
													<br><img src="<?php echo $cont->img_paragraph ?> " class="img-responsive img-bordered" style="width:100%"<>
												<?php endif ?>
												
											</div>
										</div>


									
								</p>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn-u btn-u-default" type="button">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
			<?php endforeach ?>
		</div>
		<div class="row">
			<?php $content=CatContent::model()->findAll(
				array("condition"=>" section_id in (6,7,13,14) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(16,43,36,49)",
					"order"=>"sorting")

			); ?>

			<?php if ($_SESSION['lang_selected']==1): ?>
				<div class="col-lg-12">
					<hr>
					<center><h3>Canapés Premium</h3>
						<p style="font-size:18px">10 Canapes a elegir entre dulces y Salados</p>
					</center>
					
				</div>
			<?php endif ?>
			
			<?php if ($_SESSION['lang_selected']==2): ?>
				<div class="col-lg-12">
					<hr>
					<center><h3>Premium Canapes</h3>
						<p style="font-size:18px">Choose 10 canapes between savory and sweet.</p>
					</center>
				</div>
			<?php endif ?>
			<?php foreach ($content as $cont): ?>
				<div class="col-md-3 wow fadeInUp" data-toggle="modal" data-target="#modal_<?php echo $cont->content_id ?>">
						<div class="caption" style="height:120px">
							<h3><?php echo $cont->title ?></h3>
							<p><?php echo $cont->introduction ?></p>
						</div>
						<div class="thumbnails thumbnail-style thumbnail-kenburn">
							<div class="thumbnail-img">
								<div class="overflow-hidden">
									<img class="img-responsive" src="<?php echo $cont->img_intro ?>" alt="" style="width:100%"/>
								</div>
									<a class="btn-more hover-effect">Ver más <i class="fa fa-plus-square"></i></a>
						</div>
								
						</div>
				</div>
								
					<div class="modal fade" id="modal_<?php echo $cont->content_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 id="myModalLabel1" class="modal-title"><?php echo $cont->title ?></h4>
								</div>
								<div class="modal-body" style="font-size:16px">
								<p style="font-size:24px;">
										<?php 
											$content_str=$cont->content;
											for ($i=0; $i < 100; $i++) { 
												$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
											}
										 ?>
										<?php echo $content_str; ?>
										<?php if ($cont->img_description): ?>
												<img src="<?php echo $cont->img_description ?> " class="img-responsive" style="width:80%;margin:0 auto;max-height:200px">
										<?php endif ?>
										<div class="shadow-wrapper " style="width:80%;margin:0 auto">
											<div class="box-shadow shadow-effect-2">
												<?php if ($cont->img_paragraph): ?>
													<br><img src="<?php echo $cont->img_paragraph ?> " class="img-responsive img-bordered" style="width:100%"<>
												<?php endif ?>
												
											</div>
										</div>


									
								</p>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn-u btn-u-default" type="button">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
			<?php endforeach ?>
		</div>
		<div class="row">

			<?php $content=CatContent::model()->findAll(
				array("condition"=>" section_id in (6,7,13,14) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(17,44,45,46,37,50,51,52)",
					"order"=>"sorting")

			); ?>
			<?php if ($_SESSION['lang_selected']==1): ?>
				<div class="col-lg-12">
				<hr>
				<center><h1>Catering | Cenas</h1><br><p style="font-size:18px">2 Platillos y 1 Postre a elegir</p></center><br>

			</div>
			<?php endif ?>
			
			<?php if ($_SESSION['lang_selected']==2): ?>
				<div class="col-lg-12">
				<hr>
				<center><h1>Catering | Dinners</h1><br><p style="font-size:18px">Pick 2 Dishes and 1 Dessert</p></center><br>
			</div>
			<?php endif ?>
			
			<?php foreach ($content as $cont): ?>
				<div class="col-md-3 wow fadeInUp" data-toggle="modal" data-target="#modal_<?php echo $cont->content_id ?>">
						<div class="caption" style="height:120px">
							<h3><?php echo $cont->title ?></h3>
							<p><?php echo $cont->introduction ?></p>
						</div>
						<div class="thumbnails thumbnail-style thumbnail-kenburn">
							<div class="thumbnail-img">
								<div class="overflow-hidden">
									<img class="img-responsive" src="<?php echo $cont->img_intro ?>" alt="" style="width:100%"/>
								</div>
									<a class="btn-more hover-effect">Ver más <i class="fa fa-plus-square"></i></a>
						</div>
								
						</div>
				</div>
								
					<div class="modal fade" id="modal_<?php echo $cont->content_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 id="myModalLabel1" class="modal-title"><?php echo $cont->title ?></h4>
								</div>
								<div class="modal-body" style="font-size:16px">
								<p style="font-size:24px;">
										<?php 
											$content_str=$cont->content;
											for ($i=0; $i < 100; $i++) { 
												$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
											}
										 ?>
										<?php echo $content_str; ?>
										<?php if ($cont->img_description): ?>
												<img src="<?php echo $cont->img_description ?> " class="img-responsive" style="width:80%;margin:0 auto;max-height:200px">
										<?php endif ?>
										<div class="shadow-wrapper " style="width:80%;margin:0 auto">
											<div class="box-shadow shadow-effect-2">
												<?php if ($cont->img_paragraph): ?>
													<br><img src="<?php echo $cont->img_paragraph ?> " class="img-responsive img-bordered" style="width:100%"<>
												<?php endif ?>
												
											</div>
										</div>


									
								</p>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn-u btn-u-default" type="button">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
			<?php endforeach ?>
		</div>
		<div class="row">
			<?php $content=CatContent::model()->findAll(
				array("condition"=>" section_id in (6,7,13,14) and language_id=".$_SESSION['lang_selected']." and status=TRUE and content_id in(18,19,20,38,39,40)",
					"order"=>"sorting")

			); ?>
			<?php if ($_SESSION['lang_selected']==1): ?>
				<div class="col-lg-12">
				<hr>
				<center><h1>Bebidas</h1></center><br>
			</div>
			<?php endif ?>
			
			<?php if ($_SESSION['lang_selected']==2): ?>
				<div class="col-lg-12">
				<hr>
				<center><h1>Drinks</h1></center><br>
				
			</div>
			<?php endif ?>
			
			<?php foreach ($content as $cont): ?>
				<div class="col-md-3 wow fadeInUp" data-toggle="modal" data-target="#modal_<?php echo $cont->content_id ?>">
						<div class="caption" style="height:120px">
							<h3><?php echo $cont->title ?></h3>
							<p><?php echo $cont->introduction ?></p>
						</div>
						<div class="thumbnails thumbnail-style thumbnail-kenburn">
							<div class="thumbnail-img">
								<div class="overflow-hidden">
									<img class="img-responsive" src="<?php echo $cont->img_intro ?>" alt="" style="width:100%"/>
								</div>
									<a class="btn-more hover-effect">Ver más <i class="fa fa-plus-square"></i></a>
						</div>
								
						</div>
				</div>
								
					<div class="modal fade" id="modal_<?php echo $cont->content_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 id="myModalLabel1" class="modal-title"><?php echo $cont->title ?></h4>
								</div>
								<div class="modal-body" style="font-size:16px">
								<p style="font-size:24px;">
										<?php 
											$content_str=$cont->content;
											for ($i=0; $i < 100; $i++) { 
												$content_str=str_replace("#".$i."#", "<img src='/assets/img/icons_local/".$i.".png' style='width:30px'>", $content_str);
											}
										 ?>
										<?php echo $content_str; ?>
										<?php if ($cont->img_description): ?>
												<img src="<?php echo $cont->img_description ?> " class="img-responsive" style="width:80%;margin:0 auto;max-height:200px">
										<?php endif ?>
										<div class="shadow-wrapper " style="width:80%;margin:0 auto">
											<div class="box-shadow shadow-effect-2">
												<?php if ($cont->img_paragraph): ?>
													<br><img src="<?php echo $cont->img_paragraph ?> " class="img-responsive img-bordered" style="width:100%"<>
												<?php endif ?>
												
											</div>
										</div>


									
								</p>
								</div>
								<div class="modal-footer">
									<button data-dismiss="modal" class="btn-u btn-u-default" type="button">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
			<?php endforeach ?>

			
		</div>
	</div>
<div class="text-center margin-bottom-50">
			
	</div>	
</section>

<?php endif ?>
<script type="text/javascript">

jQuery(document).ready(function() {
	$(".activable-men").first().addClass("active")
	$(".tab-pane").first().addClass("active")	
});
</script>