<?php echo $this->renderPartial("/site/header"); ?>

<body class="header-fixed header-fixed-space ">
	<div class="wrapper">
		<!--=== Header v6 ===-->
			<!-- Navbar -->
		<?php echo $this->renderPartial("/site/top_banner"); ?>
			<!-- End Navbar -->

		<!--=== End Header v6 ===-->

		
		<style type="text/css">
			.caption > p{
				font-size:17px;
			}
			.caption > h3>a{
				font-size:22px;
			}
		</style>

		<?php echo $content ?>

		<?php $this->renderPartial("/site/site_footer"); ?>
		
	</div><!--/wrapper-->

	<?php $this->renderPartial("/site/top_banner_language"); ?>

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="/assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="/assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="/assets/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="/assets/plugins/jquery.parallax.js"></script>
	<script src="/assets/plugins/master-slider/masterslider/masterslider.min.js"></script>
	<script src="/assets/plugins/master-slider/masterslider/jquery.easing.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/counter/waypoints.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/counter/jquery.counterup.min.js"></script>
	<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
	<script type="text/javascript" src="/assets/js/rhinoslider-1.05.min.js"></script>
	<script type="text/javascript" src="/assets/js/mousewheel.js"></script>
	<script type="text/javascript" src="/assets/js/easing.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="/assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="/assets/js/app.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/fancy-box.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/owl-carousel.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/master-slider-fw.js"></script>
	<script type="text/javascript" src="/assets/js/plugins/style-switcher.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			App.initCounter();
			App.initParallaxBg();
			FancyBox.initFancybox();
			MSfullWidth.initMSfullWidth();
			OwlCarousel.initOwlCarousel();
			StyleSwitcher.initStyleSwitcher();
			$('#slider').rhinoslider({
				
				controlsPlayPause: false,
				autoPlay:true,
				showTime:2800,
				effectTiema:600,

			});
			$('#slideshow').rhinoslider({
				effect: 'transfer'
			});
		});



	</script>
	<!--[if lt IE 9]>
	<script src="/assets/plugins/respond.js"></script>
	<script src="/assets/plugins/html5shiv.js"></script>
	<script src="/assets/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->
</body>
</html>
<style type="text/css">

</style>