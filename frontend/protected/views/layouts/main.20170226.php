	<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<?php echo $this->renderPartial("/site/header"); ?>

	<body id="body" data-spy="scroll" data-target=".one-page-header" class="demo-lightbox-gallery">

		<?php echo $this->renderPartial("/site/top_banner"); ?>

		<?php echo $content; ?>

		<?php echo $this->renderPartial("/site/site_contact"); ?>
		
		<script>
			jQuery(document).ready(function() {
				App.init();
				App.initCounter();
				App.initParallaxBg();
				//LoginForm.initLoginForm();
				//ContactForm.initContactForm();
				OwlCarousel.initOwlCarousel();
				StyleSwitcher.initStyleSwitcher();
				RevolutionSlider.initRSfullScreen();
			});
		</script>

	</body>
</html>