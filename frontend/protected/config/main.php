<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'password',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*','::1'),
		),
		
	),

	// application components
	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<page:\w+>'=>'<controller>/<action>',
			),
		),
		

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		// 'mail' => [
	 //         'class' => 'ext.SwiftMailer.SwiftMailer',
	 //         'transport' => [
	 //             'class' => 'Swift_SmtpTransport',
	 //             'host' => 'smtp.gmail.com',  // ej. smtp.mandrillapp.com o smtp.gmail.com
	 //             'username' => 'avm.300192@gmail.com',
	 //             'password' => 'alfalfo**',
	 //             'port' => '465', // El puerto 25 es un puerto común también
	 //             'encryption' => 'tls', // Es usado también a menudo, revise la configuración del servidor
	 //         ],
  //    	],


     	// 'mail' => array(
      //           'class' => 'ext.yii-mail.YiiMail',
      //           'transportType'=>'smtp',
      //           'transportOptions'=>array(
      //                    'host' => 'smtp.gmail.com',  // ej. smtp.mandrillapp.com o smtp.gmail.com
			   //           'username' => 'avm.300192@gmail.com',
			   //           'password' => 'alfalfo**',
			   //           'port' => '465', // El puerto 25 es un puerto común también  
			   //           'encryption' => 'ssl',                   
      //           ),
      //           'viewPath' => 'application.views.mail',             
      //   ),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
