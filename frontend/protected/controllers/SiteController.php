<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionLanding($page){
		session_start();

		if (!isset($_SESSION['lang_selected'])) {
			$_SESSION['lang_selected']=1;
			# code...
		}
		$_SESSION['ip'] 	 = $_SERVER['REMOTE_ADDR'];
		$_SESSION['host']    = gethostname();
		$_SESSION['agent']	 = $_SERVER['HTTP_USER_AGENT'];
		if (isset($_SERVER['HTTP_REFERER'])) {
			$_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
		}
		else{
			$_SESSION['referer'] ="";
		}
		$visita=new CatVisita;
		$visita->ip=$_SESSION['ip'];
		$visita->host=$_SESSION['host'];
		$visita->user_agent=$_SESSION['agent'];
		$visita->referer=$_SESSION['referer'];
		$visita->section="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$visita->save();

		$model=CatLand::model()->find(array(
				"condition"=>"language_id=".$_SESSION['lang_selected']."AND page_ref='".$page."'"."AND status=TRUE",
				"limit"=>"1",
			));
	
		$this->render('/site/pages/_std_page',array(
			
			'title'=>$model->page_title,
			'body'=>$model->html,
			'meta_content'=>$model->html_meta,
			'meta_title'=>$model->title_metadata,
			
		));
	}
	public function actionPage($page){
		
		session_start();

		if (!isset($_SESSION['lang_selected'])) {
			$_SESSION['lang_selected']=1;
			# code...
		}
		$_SESSION['ip'] 	 = $_SERVER['REMOTE_ADDR'];
		$_SESSION['host']    = gethostname();
		$_SESSION['agent']	 = $_SERVER['HTTP_USER_AGENT'];
		if (isset($_SERVER['HTTP_REFERER'])) {
			$_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
		}
		else{
			$_SESSION['referer'] ="";
		}
		$visita=new CatVisita;
		$visita->ip=$_SESSION['ip'];
		$visita->host=$_SESSION['host'];
		$visita->user_agent=$_SESSION['agent'];
		$visita->referer=$_SESSION['referer'];
		$visita->section="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$visita->save();
		$this->render('/site/'.$page);
	}
	public function actionSetLang($id){
		
		session_start();

		
		$_SESSION['lang_selected']=$id;
		

		$this->redirect('/');
		
		//echo print_r($_SESSION);
	}

	public function actionIndex()
	{
		
		session_start();

		if (!isset($_SESSION['lang_selected'])) {
			$_SESSION['lang_selected']=1;
			# code...
		}
		$_SESSION['ip'] 	 = $_SERVER['REMOTE_ADDR'];
		$_SESSION['host']    = gethostname();
		$_SESSION['agent']	 = $_SERVER['HTTP_USER_AGENT'];
		if (isset($_SERVER['HTTP_REFERER'])) {
			$_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
		}
		else{
			$_SESSION['referer'] ="";
		}
		$visita=new CatVisita;
		$visita->ip=$_SESSION['ip'];
		$visita->host=$_SESSION['host'];
		$visita->user_agent=$_SESSION['agent'];
		$visita->referer=$_SESSION['referer'];
		$visita->section="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$visita->save();

		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$this->render('index');
	}


	public function actionGenerales()
	{
		
		session_start();

		if (!isset($_SESSION['lang_selected'])) {
			$_SESSION['lang_selected']=1;
			# code...
		}

		$_SESSION['ip'] 	 = $_SERVER['REMOTE_ADDR'];
		$_SESSION['host']    = gethostname();
		$_SESSION['agent']	 = $_SERVER['HTTP_USER_AGENT'];
		if (isset($_SERVER['HTTP_REFERER'])) {
			$_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
		}
		else{
			$_SESSION['referer'] ="";
		}
		$visita=new CatVisita;
		$visita->ip=$_SESSION['ip'];
		$visita->host=$_SESSION['host'];
		$visita->user_agent=$_SESSION['agent'];
		$visita->referer=$_SESSION['referer'];
		$visita->section="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$visita->save();

		$service=CatServiceBanner::model()->find(array(
			'condition'=>"description='generalServices' and status=TRUE and language_id=".$_SESSION['lang_selected']
			));
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$this->render('site_services_detail',array('service'=>$service,'title'=>$service->name,'intro'=>$service->title,'background'=>$service->css));
	}
	public function actionEspeciales()
	{
		
		session_start();

		if (!isset($_SESSION['lang_selected'])) {
			$_SESSION['lang_selected']=1;
			# code...
		}

		$_SESSION['ip'] 	 = $_SERVER['REMOTE_ADDR'];
		$_SESSION['host']    = gethostname();
		$_SESSION['agent']	 = $_SERVER['HTTP_USER_AGENT'];
		if (isset($_SERVER['HTTP_REFERER'])) {
			$_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
		}
		else{
			$_SESSION['referer'] ="";
		}
		$visita=new CatVisita;
		$visita->ip=$_SESSION['ip'];
		$visita->host=$_SESSION['host'];
		$visita->user_agent=$_SESSION['agent'];
		$visita->referer=$_SESSION['referer'];
		$visita->section="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$visita->save();
		
		$service=CatServiceBanner::model()->find(array(
			'condition'=>"description='specialServices' and status=TRUE and language_id=".$_SESSION['lang_selected']
			));
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$this->render('site_services_detail',array('service'=>$service,'title'=>$service->name,'intro'=>$service->title,'background'=>$service->css));
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	public function genMail($name,$from,$subject,$message)
	{
		$nombre			 =$name;
		$correo_origen   =$from;
		$asunto			 =$subject;
		$mensaje         =$message;
		$correo_destino  ='avm.300192@gmail.com';
		//$this->SendMail($correo_origen,'frankvorvioz@gmail.com ',$asunto,$mensaje);
		$this->SendMail($name,$correo_origen,$correo_destino,$asunto,$mensaje);
		echo "Mensaje enviado!";
 	}
 	public function SendMail($name,$from,$to,$subject,$message)
    {  
    		
    	 	Yii::import('application.extensions.phpmailer.JPhpMailer');
			$mail=new JPhpMailer();
			$mail->IsSMTP();
			$mail->CharSet = 'utf-8';
			//$mail->SetFrom("avm.300192@gmail.com","Sitio Web");
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = '465'; 
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = "ssl";
			$mail->SMTPKeepAlive = true;  
      		$mail->Mailer = "smtp"; 
			$mail->Username = 'avm.300192@gmail.com';
			$mail->Password = 'alfalfo**';
			$mail->Subject="Centro bosques - Mensaje recibido [".$subject."]";
			$mail->MsgHTML("<html>
							<head>
							 <meta charset='UTF-8'>
							  <title>
							  </title>
							</head>
							<body>
							<table>
							".
								"<tr><td><strong>Nombre : </strong></td><td>".$name."</td><tr>".
								"<tr><td><strong>Asunto : </strong></td><td>".$subject."</td><tr>".
								"<tr><td><strong>De : </strong></td><td>".$from."</td><tr>".
								"<tr><td><strong>Mensaje : </strong></td><td>".$message."</td><tr>"
							."
							</table>
							</body>
							</html>"
				);
			$mail->AddAddress($to,"Alonso");
			$mail->Send();
			return $mail;
		}
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
			session_start();

		if (!isset($_SESSION['lang_selected'])) {
			$_SESSION['lang_selected']=1;
			# code...
		}
		$message=new CatContactMessage;
		$message->customer_name		=$_POST['name'];
		$message->customer_email 	=$_POST['email'];
		$message->customer_subject 	=$_POST['subject'];
		$message->customer_message 	=$_POST['message'];
		$message->save();
		
		// $res=$this->SendMail(
		// 	$message->customer_name,
		// 	$message->customer_email,
		// 	'avm.300192@gmail.com',
		// 	$message->customer_subject,
		// 	$message->customer_message);


		// print_r($res);
		// return 0;
		$para      = 'avm.300192@gmail.com';
		$titulo    = "Centro bosques - Mensaje recibido [".$message->customer_subject."]";
		$mensaje   = $message->customer_message;
	    $cabeceras = 'From: '. $message->customer_email. "\r\n" .
		    'Reply-To: webmaster@example.com' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();

		$res=mail($para, $titulo, $mensaje, $cabeceras);
		echo "mensaje enviado!";
		$this->redirect("/");
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}