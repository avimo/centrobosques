<?php

/**
 * This is the model class for table "centrobosques.cat_content".
 *
 * The followings are the available columns in table 'centrobosques.cat_content':
 * @property integer $content_id
 * @property string $title
 * @property string $introduction
 * @property string $content
 * @property boolean $status
 * @property integer $sorting
 * @property integer $language_id
 * @property integer $section_id
 * @property string $cc_created
 * @property string $cc_updated
 * @property string $cc_created_user
 * @property string $cc_updated_user
 * @property string $img_intro
 * @property string $img_paragraph
 * @property string $img_description
 *
 * The followings are the available model relations:
 * @property CatSection $section
 * @property CatLanguage $language
 */
class CatContent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centrobosques.cat_content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language_id', 'required'),
			array('sorting, language_id, section_id', 'numerical', 'integerOnly'=>true),
			array('cc_created_user, cc_updated_user', 'length', 'max'=>254),
			array('title, introduction, content, status, cc_created, cc_updated, img_intro, img_paragraph, img_description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('content_id, title, introduction, content, status, sorting, language_id, section_id, cc_created, cc_updated, cc_created_user, cc_updated_user, img_intro, img_paragraph, img_description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'section' => array(self::BELONGS_TO, 'CatSection', 'section_id'),
			'language' => array(self::BELONGS_TO, 'CatLanguage', 'language_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'content_id' => 'Content',
			'title' => 'Title',
			'introduction' => 'Introduction',
			'content' => 'Content',
			'status' => 'Status',
			'sorting' => 'Sorting',
			'language_id' => 'Language',
			'section_id' => 'Section',
			'cc_created' => 'Cc Created',
			'cc_updated' => 'Cc Updated',
			'cc_created_user' => 'Cc Created User',
			'cc_updated_user' => 'Cc Updated User',
			'img_intro' => 'Img Intro',
			'img_paragraph' => 'Img Paragraph',
			'img_description' => 'Img Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('content_id',$this->content_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('introduction',$this->introduction,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sorting',$this->sorting);
		$criteria->compare('language_id',$this->language_id);
		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('cc_created',$this->cc_created,true);
		$criteria->compare('cc_updated',$this->cc_updated,true);
		$criteria->compare('cc_created_user',$this->cc_created_user,true);
		$criteria->compare('cc_updated_user',$this->cc_updated_user,true);
		$criteria->compare('img_intro',$this->img_intro,true);
		$criteria->compare('img_paragraph',$this->img_paragraph,true);
		$criteria->compare('img_description',$this->img_description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatContent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
