<?php

/**
 * This is the model class for table "centrobosques.cat_service_content".
 *
 * The followings are the available columns in table 'centrobosques.cat_service_content':
 * @property integer $service_content_id
 * @property string $title
 * @property string $introduction
 * @property string $service_content
 * @property boolean $status
 * @property integer $sorting
 * @property integer $language_id
 * @property integer $service_banner_id
 * @property string $cc_created
 * @property string $cc_updated
 * @property string $cc_created_user
 * @property string $cc_updated_user
 *
 * The followings are the available model relations:
 * @property CatServiceBanner $serviceBanner
 * @property CatLanguage $language
 */
class CatServiceContent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centrobosques.cat_service_content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language_id', 'required'),
			array('sorting, language_id, service_banner_id', 'numerical', 'integerOnly'=>true),
			array('cc_created_user, cc_updated_user', 'length', 'max'=>254),
			array('title, introduction, service_content, status, cc_created, cc_updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('service_content_id, title, introduction, service_content, status, sorting, language_id, service_banner_id, cc_created, cc_updated, cc_created_user, cc_updated_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'serviceBanner' => array(self::BELONGS_TO, 'CatServiceBanner', 'service_banner_id'),
			'language' => array(self::BELONGS_TO, 'CatLanguage', 'language_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'service_content_id' => 'Service Content',
			'title' => 'Title',
			'introduction' => 'Introduction',
			'service_content' => 'Service Content',
			'status' => 'Status',
			'sorting' => 'Sorting',
			'language_id' => 'Language',
			'service_banner_id' => 'Service Banner',
			'cc_created' => 'Cc Created',
			'cc_updated' => 'Cc Updated',
			'cc_created_user' => 'Cc Created User',
			'cc_updated_user' => 'Cc Updated User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('service_content_id',$this->service_content_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('introduction',$this->introduction,true);
		$criteria->compare('service_content',$this->service_content,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sorting',$this->sorting);
		$criteria->compare('language_id',$this->language_id);
		$criteria->compare('service_banner_id',$this->service_banner_id);
		$criteria->compare('cc_created',$this->cc_created,true);
		$criteria->compare('cc_updated',$this->cc_updated,true);
		$criteria->compare('cc_created_user',$this->cc_created_user,true);
		$criteria->compare('cc_updated_user',$this->cc_updated_user,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatServiceContent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
