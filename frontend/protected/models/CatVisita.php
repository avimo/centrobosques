<?php

/**
 * This is the model class for table "clm.cat_visita".
 *
 * The followings are the available columns in table 'clm.cat_visita':
 * @property integer $visita_id
 * @property string $ip
 * @property string $host
 * @property string $user_agent
 * @property string $referer
 * @property string $section
 * @property string $cc_created
 * @property string $cc_updated
 * @property string $cc_created_user
 * @property string $cc_updated_user
 */
class CatVisita extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centrobosques.cat_visita';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cc_created_user, cc_updated_user', 'length', 'max'=>254),
			array('ip, host, user_agent, referer, section, cc_created, cc_updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('visita_id, ip, host, user_agent, referer, section, cc_created, cc_updated, cc_created_user, cc_updated_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'visita_id' => 'Visita',
			'ip' => 'Ip',
			'host' => 'Host',
			'user_agent' => 'User Agent',
			'referer' => 'Referer',
			'section' => 'Section',
			'cc_created' => 'Cc Created',
			'cc_updated' => 'Cc Updated',
			'cc_created_user' => 'Cc Created User',
			'cc_updated_user' => 'Cc Updated User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('visita_id',$this->visita_id);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('host',$this->host,true);
		$criteria->compare('user_agent',$this->user_agent,true);
		$criteria->compare('referer',$this->referer,true);
		$criteria->compare('section',$this->section,true);
		$criteria->compare('cc_created',$this->cc_created,true);
		$criteria->compare('cc_updated',$this->cc_updated,true);
		$criteria->compare('cc_created_user',$this->cc_created_user,true);
		$criteria->compare('cc_updated_user',$this->cc_updated_user,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatVisita the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
