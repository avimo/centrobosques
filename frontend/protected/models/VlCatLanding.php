<?php

/**
 * This is the model class for table "centrobosques.vl_cat_landing".
 *
 * The followings are the available columns in table 'centrobosques.vl_cat_landing':
 * @property integer $ID
 * @property string $Título
 * @property string $Sección
 * @property integer $Orden
 * @property string $Creación
 * @property string $Actualización
 * @property string $ver
 * @property string $estatus
 * @property string $editar
 */
class VlCatLanding extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centrobosques.vl_cat_landing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID, Orden', 'numerical', 'integerOnly'=>true),
			array('Sección', 'length', 'max'=>254),
			array('Título, Creación, Actualización, ver, estatus, editar', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Título, Sección, Orden, Creación, Actualización, ver, estatus, editar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Título' => 'Título',
			'Sección' => 'Sección',
			'Orden' => 'Orden',
			'Creación' => 'Creación',
			'Actualización' => 'Actualización',
			'ver' => 'Ver',
			'estatus' => 'Estatus',
			'editar' => 'Editar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Título',$this->Título,true);
		$criteria->compare('Sección',$this->Sección,true);
		$criteria->compare('Orden',$this->Orden);
		$criteria->compare('Creación',$this->Creación,true);
		$criteria->compare('Actualización',$this->Actualización,true);
		$criteria->compare('ver',$this->ver,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('editar',$this->editar,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VlCatLanding the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
