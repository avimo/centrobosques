<?php

/**
 * This is the model class for table "centrobosques.cat_land".
 *
 * The followings are the available columns in table 'centrobosques.cat_land':
 * @property integer $land_id
 * @property string $page_title
 * @property string $title
 * @property string $title_metadata
 * @property string $resume
 * @property string $html
 * @property string $html_meta
 * @property boolean $status
 * @property integer $sorting
 * @property integer $language_id
 * @property integer $section_id
 * @property string $cc_created
 * @property string $cc_updated
 * @property string $cc_created_user
 * @property string $cc_updated_user
 * @property string $img_intro
 * @property string $img_paragraph
 * @property string $img_description
 * @property string $page_ref
 * @property integer $lt_id
 *
 * The followings are the available model relations:
 * @property CatSection $section
 * @property CatLanguage $language
 * @property CatLandingType $lt
 * @property CatTopBanner[] $catTopBanners
 */
class CatLand extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centrobosques.cat_land';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('language_id', 'required'),
			array('sorting, language_id, section_id, lt_id', 'numerical', 'integerOnly'=>true),
			array('cc_created_user, cc_updated_user', 'length', 'max'=>254),
			array('page_title, title, title_metadata, resume, html, html_meta, status, cc_created, cc_updated, img_intro, img_paragraph, img_description, page_ref', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('land_id, page_title, title, title_metadata, resume, html, html_meta, status, sorting, language_id, section_id, cc_created, cc_updated, cc_created_user, cc_updated_user, img_intro, img_paragraph, img_description, page_ref, lt_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'section' => array(self::BELONGS_TO, 'CatSection', 'section_id'),
			'language' => array(self::BELONGS_TO, 'CatLanguage', 'language_id'),
			'lt' => array(self::BELONGS_TO, 'CatLandingType', 'lt_id'),
			'catTopBanners' => array(self::HAS_MANY, 'CatTopBanner', 'land_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'land_id' => 'Land',
			'page_title' => 'Page Title',
			'title' => 'Title',
			'title_metadata' => 'Title Metadata',
			'resume' => 'Resume',
			'html' => 'Html',
			'html_meta' => 'Html Meta',
			'status' => 'Status',
			'sorting' => 'Sorting',
			'language_id' => 'Language',
			'section_id' => 'Section',
			'cc_created' => 'Cc Created',
			'cc_updated' => 'Cc Updated',
			'cc_created_user' => 'Cc Created User',
			'cc_updated_user' => 'Cc Updated User',
			'img_intro' => 'Img Intro',
			'img_paragraph' => 'Img Paragraph',
			'img_description' => 'Img Description',
			'page_ref' => 'Page Ref',
			'lt_id' => 'Lt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('land_id',$this->land_id);
		$criteria->compare('page_title',$this->page_title,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_metadata',$this->title_metadata,true);
		$criteria->compare('resume',$this->resume,true);
		$criteria->compare('html',$this->html,true);
		$criteria->compare('html_meta',$this->html_meta,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sorting',$this->sorting);
		$criteria->compare('language_id',$this->language_id);
		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('cc_created',$this->cc_created,true);
		$criteria->compare('cc_updated',$this->cc_updated,true);
		$criteria->compare('cc_created_user',$this->cc_created_user,true);
		$criteria->compare('cc_updated_user',$this->cc_updated_user,true);
		$criteria->compare('img_intro',$this->img_intro,true);
		$criteria->compare('img_paragraph',$this->img_paragraph,true);
		$criteria->compare('img_description',$this->img_description,true);
		$criteria->compare('page_ref',$this->page_ref,true);
		$criteria->compare('lt_id',$this->lt_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatLand the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
