<?php

/**
 * This is the model class for table "centrobosques.cat_contact_message".
 *
 * The followings are the available columns in table 'centrobosques.cat_contact_message':
 * @property integer $contact_message_id
 * @property string $title
 * @property string $customer_name
 * @property string $customer_subject
 * @property string $customer_email
 * @property string $customer_message
 * @property string $cc_created
 * @property string $cc_updated
 * @property string $cc_created_user
 * @property string $cc_updated_user
 */
class CatContactMessage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centrobosques.cat_contact_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cc_created_user, cc_updated_user', 'length', 'max'=>254),
			array('title, customer_name, customer_subject, customer_email, customer_message, cc_created, cc_updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('contact_message_id, title, customer_name, customer_subject, customer_email, customer_message, cc_created, cc_updated, cc_created_user, cc_updated_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'contact_message_id' => 'Contact Message',
			'title' => 'Title',
			'customer_name' => 'Customer Name',
			'customer_subject' => 'Customer Subject',
			'customer_email' => 'Customer Email',
			'customer_message' => 'Customer Message',
			'cc_created' => 'Cc Created',
			'cc_updated' => 'Cc Updated',
			'cc_created_user' => 'Cc Created User',
			'cc_updated_user' => 'Cc Updated User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('contact_message_id',$this->contact_message_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('customer_name',$this->customer_name,true);
		$criteria->compare('customer_subject',$this->customer_subject,true);
		$criteria->compare('customer_email',$this->customer_email,true);
		$criteria->compare('customer_message',$this->customer_message,true);
		$criteria->compare('cc_created',$this->cc_created,true);
		$criteria->compare('cc_updated',$this->cc_updated,true);
		$criteria->compare('cc_created_user',$this->cc_created_user,true);
		$criteria->compare('cc_updated_user',$this->cc_updated_user,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatContactMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
