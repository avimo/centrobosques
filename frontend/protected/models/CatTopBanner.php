<?php

/**
 * This is the model class for table "centrobosques.cat_top_banner".
 *
 * The followings are the available columns in table 'centrobosques.cat_top_banner':
 * @property integer $top_banner_id
 * @property string $name
 * @property string $description
 * @property string $title
 * @property string $breadcumbs
 * @property string $link
 * @property string $css
 * @property boolean $status
 * @property integer $sorting
 * @property integer $top_banner_parent_id
 * @property integer $language_id
 * @property string $cc_created
 * @property string $cc_updated
 * @property string $cc_created_user
 * @property string $cc_updated_user
 * @property string $icon
 * @property integer $land_id
 *
 * The followings are the available model relations:
 * @property CatSection[] $catSections
 * @property CatLanguage $language
 * @property CatTopBanner $topBannerParent
 * @property CatTopBanner[] $catTopBanners
 * @property CatLand $land
 */
class CatTopBanner extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centrobosques.cat_top_banner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description', 'required'),
			array('sorting, top_banner_parent_id, language_id, land_id', 'numerical', 'integerOnly'=>true),
			array('name, title, breadcumbs, cc_created_user, cc_updated_user', 'length', 'max'=>254),
			array('icon', 'length', 'max'=>250),
			array('link, css, status, cc_created, cc_updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('top_banner_id, name, description, title, breadcumbs, link, css, status, sorting, top_banner_parent_id, language_id, cc_created, cc_updated, cc_created_user, cc_updated_user, icon, land_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catSections' => array(self::HAS_MANY, 'CatSection', 'top_banner_id'),
			'language' => array(self::BELONGS_TO, 'CatLanguage', 'language_id'),
			'topBannerParent' => array(self::BELONGS_TO, 'CatTopBanner', 'top_banner_parent_id'),
			'catTopBanners' => array(self::HAS_MANY, 'CatTopBanner', 'top_banner_parent_id'),
			'land' => array(self::BELONGS_TO, 'CatLand', 'land_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'top_banner_id' => 'Top Banner',
			'name' => 'Name',
			'description' => 'Description',
			'title' => 'Title',
			'breadcumbs' => 'Breadcumbs',
			'link' => 'Link',
			'css' => 'Css',
			'status' => 'Status',
			'sorting' => 'Sorting',
			'top_banner_parent_id' => 'Top Banner Parent',
			'language_id' => 'Language',
			'cc_created' => 'Cc Created',
			'cc_updated' => 'Cc Updated',
			'cc_created_user' => 'Cc Created User',
			'cc_updated_user' => 'Cc Updated User',
			'icon' => 'Icon',
			'land_id' => 'Land',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('top_banner_id',$this->top_banner_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('breadcumbs',$this->breadcumbs,true);
		$criteria->compare('link',$this->link,true);
		$criteria->compare('css',$this->css,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sorting',$this->sorting);
		$criteria->compare('top_banner_parent_id',$this->top_banner_parent_id);
		$criteria->compare('language_id',$this->language_id);
		$criteria->compare('cc_created',$this->cc_created,true);
		$criteria->compare('cc_updated',$this->cc_updated,true);
		$criteria->compare('cc_created_user',$this->cc_created_user,true);
		$criteria->compare('cc_updated_user',$this->cc_updated_user,true);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('land_id',$this->land_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CatTopBanner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
